local pullEvent = os.pullEvent
--os.pullEvent = os.pullEventRaw
--[[
	
	ACI Package Online Installer
	Copyright (c) Peekofwar 2020
	
	v1.0.0 (Jul 7, 2020)
	v1.0.1 (Aug 21, 2020)
	+ Added pcall error screen
	+ Added error message for incorrect arguments
	  when using force color '/f' switch
	+ Tweaked hidden (experimental) diskette code
	  (use '/dev' switch to access)
	v1.0.1b (Nov. 17, 2020)
	* Switched addresses to Gitlab files

]]
version = "ACI Package Online Installer (v1.0.1b)"

args = { ... }

function checkArgs()
	arg = {}
	local int = 0
	while int < #args do
		int = int + 1
		if args[int] == '/f' then
			if args[int+1] == 'm' then arg.forceMono = true
			elseif args[int+1] == 'mono' then arg.forceMono = true
			elseif args[int+1] == 'c' then arg.forceCol = true
			elseif args[int+1] == 'color' then arg.forceCol = true
			elseif args[int+1] == 'colour' then arg.forceCol = true
			else
				setUIColors()
				print('')
				sCol('t',col.err)
				print('Invalid argument for switch \'/f\' (force color)')
				sCol('t',col.w)
				print('\nAvailable arguments:\nm, mono, c, color, colour\n')
				error()
			end
			int = int + 1
		elseif args[int] == '/?' or args[int] == '/help' or args[int] == '/h' then
			arg.showHelp = true
			print('')
			print('Available switches:')
			print(' \'/f <mono or color>\' forces color or monochrome set')
			print(' \'/?\' shows help screen')
			print('')
			error()
		elseif args[int] == '/dev' then
			arg.devMode = true
			sPos(1,1) term.clear()
			print('\nDeveloper mode enabled.\n\nHidden experimental features are unlocked.')
			sleep(1)
		end
	end
	if not arg.devMode then os.pullEvent = os.pullEventRaw end
end

programList_web = {}
programList_dest = {}
programInstaller = {}
table.insert(programList_web, 1, "https://gitlab.com/peekofwar-craftos-programs/bios-gui-setup-program/-/raw/master/CraftOS_BIOS_Utility_Program.lua")
table.insert(programList_dest, 1, "/.bios.lua")
table.insert(programList_web, 2, "https://gitlab.com/peekofwar-craftos-programs/password-gui-program/-/raw/master/CraftOS_Password_Utility_Program.lua")
table.insert(programList_dest, 2, "/.systemPassword.lua")
table.insert(programList_web, 3, "https://gitlab.com/peekofwar-craftos-programs/basic-startup-package/-/raw/master/Boot_Screen.lua")
table.insert(programList_dest, 3, "/startup.lua")
programList_tempDir = '/.temp/startup_package'
programList_diskDir = '/disk/startup_package'

programInstaller.web = "https://gitlab.com/peekofwar-craftos-programs/basic-startup-package/-/raw/master/Offline_Installer.lua"
programInstaller.dest = "/install.lua"
programInstaller.helpweb = "https://gitlab.com/peekofwar-craftos-programs/basic-startup-package/-/raw/master/Offline_Installer_Help_File.txt"
programInstaller.helpdest = "/help.txt"

local term_w,term_h = term.getSize()
function compatibilityCheck()
	if term_w ~= 51 or term_h ~= 19 then
		key = nil
		sCol("t",col.err)
		print("Note: This program and the programs it installs are designed for a screen with 51 columbs and 19 rows. It may not display correctly for your device.\n\nPress any key to continue.")
		while not key do
			sleep(0.01)
		end
		key = nil
	end
end
function clamp(vMin,vMax,x)
	return math.max(math.min(x,vMax),vMin)
end
function setUIColors()
	col = {}
	if term.isColor() and not arg.forceMono or arg.forceCol then
		col.tabT = 256			-- Tab text fore
		col.tabB = 2048			-- Tab text back
		col.tabF = 2048			-- Tab line
		col.headT = 32768		-- Header text
		col.headB = 512			-- Header fill
		col.fill = 256			-- Background fill
		col.item = 32768		-- Item header
		col.itemS = 1			-- Selected item
		col.itemE = 2048		-- Selectable (enabled) item
		col.itemD = 128			-- Not-selectable (disabled) item
		col.msg = 16			-- Message text
		col.err = 16384			-- Error text
		
		col.dialogT = 32768		-- Dialog box text
		col.dialogB = 1			-- Dialog box fill
		
		col.progBarF = 2048
		col.progBarB = 128
		
		col.w = 1
		col.b = 32768
	else -- Monochrome compatible color set:
		col.tabT = 256			-- Tab text fore
		col.tabB = 128			-- Tab text back
		col.tabF = 128			-- Tab line
		col.headT = 32768		-- Header text
		col.headB = 256			-- Header fill
		col.fill = 32768		-- Background fill
		col.item = 256			-- Item header
		col.itemS = 1			-- Selected item
		col.itemE = 256			-- Selectable (enabled) item
		col.itemD = 128			-- Not-selectable (disabled) item
		col.msg = 1				-- Message text
		col.err = 1				-- Error text
		
		col.dialogT = 1			-- Dialog box text
		col.dialogB = 128		-- Dialog box fill
		
		col.progBarF = 256
		col.progBarB = 128
		
		col.w = 1
		col.b = 32768
	end
end
--	Set color
function sCol(back, color)
	if back == "t" then
		term.setTextColor(color)
		return true, "Fore"
	else
		term.setBackgroundColor(color)
		return true, "Back"
	end
end
--	Set position relative, and Y-Axis relative only
function sPos(x, y, relative, r2)
	if relative then
		x1, y1 = term.getCursorPos()
		if not r2 then x = x1 + x end --Allows excluding X from relative
		y = y1 + y
	end
	return term.setCursorPos(x, y)
end
--	Center write
local function cWrite(text)
    local w, h = term.getSize()
	local cX,cY = term.getCursorPos()
    term.setCursorPos(math.floor(w / 2 - #text / 2 + .5), cY)
    io.write(text)
end
--	Dialog background printer (prerequisites: 'cWrite()', 'sPos()', 'sCol')
function dialog(lengthmode,headline)
	sPos(1,6) if lengthmode == 1 then sPos(0,-1,true) end sCol("b", col.headB) sCol("t", col.headT)
	cWrite("                                  ") cWrite(headline) sPos(0,1,true) sCol("b", col.dialogB) sCol("t", col.dialogT)
	cWrite("                                  ") sPos(0,1,true)
	cWrite("                                  ") sPos(0,1,true)
	cWrite("                                  ") sPos(0,1,true)
	cWrite("                                  ") sPos(0,1,true)
	cWrite("                                  ") sPos(0,1,true)
	cWrite("                                  ") sPos(0,1,true)
	cWrite("                                  ") sPos(0,1,true)
	cWrite("                                  ") sPos(0,1,true) if lengthmode == 1 then
	cWrite("                                  ") sPos(0,1,true)
	cWrite("                                  ") sPos(0,1,true) end
end
--	Progress bar (Requires 'sPos()')
function progressBar(x, y, width, items, completed, phase, noNumbers)
	local sw, sh = term.getSize()
	--local percent = math.max(math.min(items / completed * 100,100),0)
	local percent =clamp(0,100,items / completed * 100)
	local oldColor = term.getBackgroundColor()
	local phaseText = phase
	local itemscompleted = ' '..items..'/'..completed
	if noNumbers then itemscompleted = '' end
	if phaseText and #phaseText > width-#itemscompleted-3 then
		phaseText = string.sub(phaseText,1,width-#itemscompleted-3)..'...'
	end
	
	sPos(x, y-1) if phase ~= nil then paintutils.drawLine(x,y-1,x+width-1,y-1) sPos(x, y-1) write(phaseText) sPos(x+width-#itemscompleted,y-1) write(itemscompleted) end
	sCol('b',col.progBarB)
	paintutils.drawLine(x,y,width+x-1,y)
	sCol('b',col.progBarF)
	if percent/100*width-1 > 0 and width >= x then
		paintutils.drawLine(x,y,percent/100*width+x-1,y)
	end
	sCol('b',oldColor)
end
function drawScreen(rrst)
	tabName = {}
	table.insert(tabName,1,' Main ')
	table.insert(tabName,2,' Install ')
	table.insert(tabName,3,' Finish ')
	
	local tX, tY = term.getSize()
	
	sCol("b", col.fill)
	sPos(1,1) term.clear()
	
	sPos(1,1) sCol("b", col.headB) term.clearLine() sCol("t", col.headT) sPos(5,1) cWrite(version)
	
	sPos(1,2) sCol("b", col.tabF) term.clearLine()
	if tab == 1 then
		sCol("b",col.tabT) sCol("t",col.tabB)
	else
		sCol("t",col.tabT) sCol("b",col.tabB)
	end
	sPos(2, 2) write(tabName[1])
	if tab == 2 then
		sCol("b",col.tabT) sCol("t",col.tabB)
	else
		sCol("t",col.tabT) sCol("b",col.tabB)
	end
	sPos(2+#tabName[1], 2) write(tabName[2])
	if tab == 3 then
		sCol("b",col.tabT) sCol("t",col.tabB)
	else
		sCol("t",col.tabT) sCol("b",col.tabB)
	end
	sPos(2+#tabName[1]+#tabName[2], 2) write(tabName[3])
	if tab == 4 then
		sCol("b",col.tabT) sCol("t",col.tabB)
	else
		sCol("t",col.tabT) sCol("b",col.tabB)
	end
	sPos(22, 2) write("")
	
	if rrst then dispRS = nil end
	
	sCol("t", col.headT)
	sPos(1,tY-1) sCol("b", col.headB) term.clearLine()
	if tab == 1 then
		sPos(1,tY) sCol("b", col.headB) term.clearLine() sPos(2,tY) write("/\\/ Navigate") sPos(16,tY) cWrite("") sPos(39,tY) write("ENTER Select")
	else
		sPos(1,tY) sCol("b", col.headB) term.clearLine() sPos(2,tY) write("") sPos(16,tY) cWrite("") sPos(39,tY) write("")
	end
	if arg.devMode then cWrite("DEV MODE") end

	
	if tab == 1 then
		menuMain(rrst)
	elseif tab == 2 then
		menuInstall(rrst)
	elseif tab == 3 then
		menuFinish(rrst)
	elseif tab == 4 then
	--	menuExit(rrst)
	end
	
end
function menuMain(rrst)
	sCol("t", col.item)
	sCol("b", col.fill)
	
--				   |                                                   |
	sPos(2,4) write("This package contains a BIOS setup utility, a")
	sPos(2,5) write("password program, and a startup splash screen.")
	
	sPos(2,7) write("This will overwrite any previous installation.")

	if rrst then row = 2 end
	if arg.devMode then 
		if row == 1 then sCol('t',col.itemS) else sCol('t',col.itemE) end
	else
		if row == 1 then sCol('t',col.itemS) else sCol('t',col.itemD) end
	end
	sPos(2,10) cWrite('Download installer to diskette')
	if row == 2 then sCol('t',col.itemS) else sCol('t',col.itemE) end
	sPos(2,11) cWrite('Install on this machine')
	if row == 3 then sCol('t',col.itemS) else sCol('t',col.itemE) end
	sPos(2,12) cWrite('Quit')

end
function menuInstall(rrst)
	key = nil
	if isDisketteInstall and not fs.exists('/disk') then
		dialog(1,'Diskette Error')
		while true do
			sPos(1,9) cWrite('Insert diskette and press')
			sPos(1,10) cWrite('ENTER to continue.')
			sPos(1,12) cWrite('Press [C] to cancel.')
			if key == keys.c then
				abort = true
				isDisketteInstall = false
				tab = 1
				drawScreen(true)	
				break
			elseif key == keys.enter and fs.exists('/disk') then
				sCol("t", col.item)
				sCol("b", col.fill)
				paintutils.drawFilledBox(1,3,51,17)
				break
			elseif key == keys.enter and not fs.exists('/disk') then
				key = nil
			end
			sleep(0.01)
		end
	end
	if isDisketteInstall and fs.exists('/disk') then
		key = nil
		local disketteFiles = fs.list('/disk')
		if #disketteFiles > 0 then
			dialog(1,'Disk Not Empty')
			while true do
				sPos(1,8) cWrite('The disk insertted has files')
				sPos(1,9) cWrite('on it.')
				sPos(1,11) cWrite('Do you want to wipe it?')
				sPos(1,12) cWrite('[N]  [Y]')
				if key == keys.n then
					sCol("t", col.item)
					sCol("b", col.fill)
					paintutils.drawFilledBox(1,3,51,17)
					break
				elseif key == keys.y and fs.exists('/disk') then
					sCol("t", col.item)
					sCol("b", col.fill)
					dialog(1,'Wiping Disk')
					progressBar(10,14,32,0,#disketteFiles,'Removing files...')
					isCleanDiskette = true
					break
				end
				sleep(0.01)
			end
		end

		if isCleanDiskette then
			local int = 0
			while int < #disketteFiles do
				int = int + 1
				progressBar(10,14,32,int,#disketteFiles,'Removing: '..disketteFiles[int])
				if fs.exists('/disk/'..disketteFiles[int]) then fs.delete('/disk/'..disketteFiles[int]) end
				sleep(0.1)
			end
			sCol("t", col.item)
			sCol("b", col.fill)
			paintutils.drawFilledBox(1,3,51,17)
		end
 	end
	sleep(0)
	if not abort then
		if isDisketteInstall then
			table.insert(programList_web, programInstaller.web)
			table.insert(programList_dest, programInstaller.dest)
			table.insert(programList_web, programInstaller.helpweb)
			table.insert(programList_dest, programInstaller.helpdest)
		end
		
		local installer_phaseDelay = 0.2
		sCol("t", col.item)
		sCol("b", col.fill)
		int = 0
		progressBar(2,16,49,int,#programList_dest,'Downloading...')
		sPos(2,4) term.clearLine() write('Downloading files...')
		sleep(installer_phaseDelay)
		
		while int < #programList_dest do
			int = int + 1
			paintutils.drawFilledBox(1,5,51,11)
			sPos(2,6) shell.run('wget '..programList_web[int]..' '..programList_tempDir..programList_dest[int])
			progressBar(2,16,49,int,#programList_dest,'Downloading: '..programList_dest[int])
			sleep(0.1)
		end
		
		int = 0
		paintutils.drawFilledBox(1,5,51,11)
		progressBar(2,16,49,int,#programList_dest,'Verifying...')
		sPos(2,4) term.clearLine() write('Verifying files...')
		sleep(installer_phaseDelay)
		
		programList_verify = {}
		while int < #programList_dest do
			int = int + 1
			sPos(2,6) table.insert(programList_verify,int,fs.exists(programList_tempDir..programList_dest[int]))
			sPos(2,6) if not fs.exists(programList_tempDir..programList_dest[int]) then sCol('t',col.err) write('File(s) missing!') downloadError = true sCol('t',col.item) end
			progressBar(2,16,49,int,#programList_dest,'Verifyng: '..programList_dest[int])
			sleep(0.1)
		end

		if not downloadError then
			
			int = 0
			paintutils.drawFilledBox(1,5,51,11)
			progressBar(2,16,49,int,#programList_dest,'Deleting...')
			sPos(2,4) term.clearLine() write('Removing previous installation...')
			sleep(installer_phaseDelay)
			
			while int < #programList_dest do
				int = int + 1
				if isDisketteInstall then
					if programList_dest[int] == programInstaller.dest or programList_dest[int] == programInstaller.helpdest then
						sPos(2,4) if fs.exists('/disk'..programList_dest[int]) then fs.delete('/disk'..programList_dest[int]) end
					else
						sPos(2,4) if fs.exists(programList_diskDir..programList_dest[int]) then fs.delete(programList_diskDir..programList_dest[int]) end
					end
				else
					sPos(2,4) if fs.exists(programList_dest[int]) then fs.delete(programList_dest[int]) end
				end
				progressBar(2,16,49,int,#programList_dest,'Deleting: '..programList_dest[int])
				sleep(0.1)
			end

			int = 0
			paintutils.drawFilledBox(1,5,51,11)
			if isDisketteInstall then
				progressBar(2,16,49,int,#programList_dest,'Moving...')
				sPos(2,4) term.clearLine() write('Moving to disk...')
			else
				progressBar(2,16,49,int,#programList_dest,'Installing...')
				sPos(2,4) term.clearLine() write('Installing programs...')
			end
			sleep(installer_phaseDelay)
			
			while int < #programList_dest do
				int = int + 1
				if isDisketteInstall then
					if programList_dest[int] == programInstaller.dest or programList_dest[int] == programInstaller.helpdest then
						sPos(2,4) fs.move(programList_tempDir..programList_dest[int],'/disk'..programList_dest[int])
					else
						sPos(2,4) fs.move(programList_tempDir..programList_dest[int],programList_diskDir..programList_dest[int])
					end
				else
					sPos(2,4) fs.move(programList_tempDir..programList_dest[int],programList_dest[int])
				end
				if isDisketteInstall then
					progressBar(2,16,49,int,#programList_dest,'Moving: '..programList_dest[int])
				else
					progressBar(2,16,49,int,#programList_dest,'Installing: '..programList_dest[int])
				end
				sleep(0.1)
			end
		end
		key = nil
		tab = 3
		drawScreen(true)
	end
	abort = nil
end
function menuFinish(rrst)
	sCol("t", col.item)
	sCol("b", col.fill)
	key = nil
	if downloadError then
		dialog(0,'Download Error')
		sPos(1,8) cWrite('One or more files failed to')
		sPos(1,9) cWrite('download.')
		sPos(1,11) cWrite('Press any key to quit.')
		sPos(1,12) cWrite('Press F1 for details.')
	else
	--				   |                                                   |
		if isDisketteInstall then
			sPos(2,4) write('Installation finished.')
			sPos(2,6) write('Files downloaded to diskette.')
			sPos(2,8) write('Note: Currently there is no local installer.')
			sPos(2,16) write('Press any key to continue.')
		else
			sPos(2,4) write('Installation finished.')
			sPos(2,6) write('Run \'.systemPassword\' to set a password for')
			sPos(2,7) write('your PC.')
			sPos(2,16) write('Press any key to continue.')
		end
	end
end

function listen()
	listenBreak = false
	while not listenBreak do
		event, key = os.pullEvent( "key" )
	end
end
function core()
	compatibilityCheck()
	tab = 1
	drawScreen(true)
	while true do
		if event == "terminate" and arg.devMode then sCol('t',col.err) sCol('b',col.b) write('X') sPos(1,1) print("Program terminated") error() end
		if tab == 1 then
			if key == keys.up then
				row = clamp(1,3,row - 1)
				if not arg.devMode and row == 1 then row = 2 end
				drawScreen()
				key = nil
			elseif key == keys.down then
				row = clamp(1,3,row + 1)
				if not arg.devMode and row == 1 then row = 2 end
				drawScreen()
				key = nil
			elseif key == keys.enter then
				if row == 1 then 
					isDisketteInstall = true
					tab = 2
					drawScreen(true)
				elseif row == 2 then
					isDisketteInstall = false
					tab = 2
					drawScreen(true)
				elseif row == 3 then
					quit() 
				end
			end
		elseif tab == 3 then
			if key and key ~= keys.f1 then
				quit()
			elseif key == keys.f1 and downloadError then
				sPos(1,1)
				sCol('t',col.w)
				sCol('b',col.b)
				term.clear()
				print('Failed to download:')
				local int = 0
				while int < #programList_verify do
					int = int + 1
					if programList_verify[int] == false then 
						print('  '..programList_dest[int]) 
					end
				end
				print('')
				quit(false, true)
			end
		end
		sleep(0.01)
	end
end
function quit(warn, noClear)
	sCol("t",col.w)
	sCol("b",col.b)
	if not noClear then
		sPos(1,1)
		term.clear()
	end
	os.pullEvent = pullEvent
	error()
end
checkArgs()
setUIColors()
function start_program()
	parallel.waitForAll(core, listen)
end
if arg.devMode then
	parallel.waitForAll(core, listen)
else
	pass, err = pcall(start_program)
	if not pass and err then
		sPos(1,2)
		sCol('b',col.b)
		sCol('t',col.err)
		term.clear()
		if err == nil then err = '( no error given )' end
		print('Something went wromg!\n\n'..err..'\n')
		os.pullEvent = pullEvent
	end
end

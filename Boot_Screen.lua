local pullEvent = os.pullEvent
os.pullEvent = os.pullEventRaw
SCAN = 0
term.clear()

--[[
	Startup script made by Peekofwar
	Version 1.0.2
	
	A simple boot screen with a nice
	graphical splash screen. Will also
	execute the ACI Password program and
	the ACI BIOS Setup program if they're
	installed.
	
	Changelog:
	v1.0.1 (Oct 9, 2019 @ 3:09 PM EST)
	(unlogged)
	
	v1.0.2 (Dec 19, 2019 @ 10:53 AM EST)
	+ New boot screen splash image
]]
term.setCursorPos(1,1)
term.setCursorBlink(true)
sleep(1)
if fs.exists("/.bios.lua") then
	shell.run("/.bios.lua /b")
end
term.setCursorPos(1,1)
term.setCursorBlink(false)
sleep(0.25)
term.setCursorBlink(true)
sleep(0.5)

term.setCursorPos(1,1)
term.clear()

term.setCursorBlink(false)

if term.isColor() then
	term.setBackgroundColor(2048)
	term.setTextColor(1)
end

term.setCursorPos(1,5)
term.clear()

	--51 wide, 19 tall	
term.setCursorPos(1,1)
if term.isColor() then
	term.setTextColor(16)
	term.setBackgroundColor(32768)
end

term.clear()
		-- Boot Logo Screen
if term.isColor() then
	--print("insert color logo here")
	if fs.exists("/.osLogo.nfp") then
		local os_logo = paintutils.loadImage("/.osLogo.nfp")
		paintutils.drawImage(os_logo,1,1)
	else
		local defaultLogo = {}
		
		table.insert(defaultLogo,"fffffffffffffffffffffffffffffffffffffffffffffffffff")
		table.insert(defaultLogo,"fffffffffffffffffffffffffffffffffffffffffffffffffff")
		table.insert(defaultLogo,"fffffffffffffffffffffffffffffffffffffffffffffffffff")
		table.insert(defaultLogo,"444444444444444444444444444444444444444444444444444")
		table.insert(defaultLogo,"44444444ff44fff4444ff444fff44fff4444444444444444444")
		table.insert(defaultLogo,"4444444f4444f44f44f44f44f44444f44444444444444444444")
		table.insert(defaultLogo,"4444444f4444fff444ffff44ff4444f44444444444444444444")
		table.insert(defaultLogo,"4444444f4444f44f44f44f44f44444f44444444444444444444")
		table.insert(defaultLogo,"44444444ff44f44f44f44f44f44444f44444444444444444444")
		table.insert(defaultLogo,"444444444444444444444444444444444444444444444444444")
		table.insert(defaultLogo,"fffffffffffffffffffffffffffffffffffffffffffffffffff")
		table.insert(defaultLogo,"fffffffffffffffffffffffffffffffffff44ffff444fffffff")
		table.insert(defaultLogo,"ffffffffffffffffffffffffffffffffff4ff4ff4ffffffffff")
		table.insert(defaultLogo,"ffffffffffffffffffffffffffffffffff4ff4fff44ffffffff")
		table.insert(defaultLogo,"ffffffffffffffffffffffffffffffffff4ff4fffff4fffffff")
		table.insert(defaultLogo,"fffffffffffffffffffffffffffffffffff44fff444ffffffff")
		table.insert(defaultLogo,"fffffffffffffffffffffffffffffffffffffffffffffffffff")
		table.insert(defaultLogo,"fffffffffffffffffffffffffffffffffffffffffffffffffff")
		table.insert(defaultLogo,"fffffffffffffffffffffffffffffffffffffffffffffffffff")
		
		file = fs.open("/.osLogo.nfp","a")
		local int = 1
		while int <= 19 do
			file.writeLine(defaultLogo[int])
			int = int + 1
		end
		file.close()
		
		if fs.exists("/.osLogo.nfp") then
			local os_logo = paintutils.loadImage("/.osLogo.nfp")
			paintutils.drawImage(os_logo,1,1)
		else
			print("Failed to load default logo image.")
		end
	end
else
	--print("insert monochrome logo here")
	if fs.exists("/.osLogo_mono.nfp") then
		local os_logo_mono = paintutils.loadImage("/.osLogo_mono.nfp")
		paintutils.drawImage(os_logo_mono,1,1)
	else
		local defaultLogo = {}
		
		table.insert(defaultLogo,"fffffffffffffffffffffffffffffffffffffffffffffffffff")
		table.insert(defaultLogo,"fffffffffffffffffffffffffffffffffffffffffffffffffff")
		table.insert(defaultLogo,"fffffffffffffffffffffffffffffffffffffffffffffffffff")
		table.insert(defaultLogo,"888888888888888888888888888888888888888888888888888")
		table.insert(defaultLogo,"88888888ff88fff8888ff888fff88fff8888888888888888888")
		table.insert(defaultLogo,"8888888f8888f88f88f88f88f88888f88888888888888888888")
		table.insert(defaultLogo,"8888888f8888fff888ffff88ff8888f88888888888888888888")
		table.insert(defaultLogo,"8888888f8888f88f88f88f88f88888f88888888888888888888")
		table.insert(defaultLogo,"88888888ff88f88f88f88f88f88888f88888888888888888888")
		table.insert(defaultLogo,"888888888888888888888888888888888888888888888888888")
		table.insert(defaultLogo,"fffffffffffffffffffffffffffffffffffffffffffffffffff")
		table.insert(defaultLogo,"fffffffffffffffffffffffffffffffffff88ffff888fffffff")
		table.insert(defaultLogo,"ffffffffffffffffffffffffffffffffff8ff8ff8ffffffffff")
		table.insert(defaultLogo,"ffffffffffffffffffffffffffffffffff8ff8fff88ffffffff")
		table.insert(defaultLogo,"ffffffffffffffffffffffffffffffffff8ff8fffff8fffffff")
		table.insert(defaultLogo,"fffffffffffffffffffffffffffffffffff88fff888ffffffff")
		table.insert(defaultLogo,"fffffffffffffffffffffffffffffffffffffffffffffffffff")
		table.insert(defaultLogo,"fffffffffffffffffffffffffffffffffffffffffffffffffff")
		table.insert(defaultLogo,"fffffffffffffffffffffffffffffffffffffffffffffffffff")
		
		file = fs.open("/.osLogo_mono.nfp","a")
		local int = 1
		while int <= 19 do
			file.writeLine(defaultLogo[int])
			int = int + 1
		end
		file.close()
		
		if fs.exists("/.osLogo_mono.nfp") then
			local os_logo_mono = paintutils.loadImage("/.osLogo_mono.nfp")
			paintutils.drawImage(os_logo_mono,1,1)
		else
			print("Failed to load default logo image.")
		end
	end
		
end
sleep(2)

term.setCursorPos(1,1)

if term.isColor() then
	term.setTextColor(1)
	term.setBackgroundColor(32768)
end

term.clear()
os.pullEvent = pullEvent

--BOOT PROGRAM
if fs.exists("/.systemPassword.lua") then
	shell.run("/.systemPassword.lua /b")
end

--If no boot programs, then this will run.
shell.run("shell")
os.shutdown()
